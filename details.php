<!-- REST Connection Start -->
<?php
 
  session_start();
 
  if(empty($_SESSION))
      header('Location: index.php');
 
  require_once('executeREST.php');
  $TOKEN = $_SESSION['access_token'];
  $url = $_SESSION['url'].'/api/1.0/tasks?page=1&include=user,assignableUsers&process_request_id='.$_COOKIE['selectedID'];
  $response = executeREST( $url, 'GET', array(), $_SESSION['access_token'] );
 // var_dump($response);

$json_string=json_encode($response,JSON_PRETTY_PRINT);

?>
<!-- REST Connection End -->

<?php include_once("header.html"); ?>
<!-- Page Content Start-->
        <div>
             <h3>TASK INBOX MIDDLE STEP (<?php echo count($response["data"]); ?>)</h3>
             <table class="table table-hover table-bordered">
               <thead><tr class="bg-primary">
                <th>#</th>
                <th>Task ID</th>
                <th>Task Name</th>
                <th>Status</th>
                <th>Initiated</th>
                <th>Due</th>
                <th></th></tr></thead>
                <?php
                $caseList = $response["data"];
                    $i = 0;
                    foreach($caseList as $case){
                    $i++;
                    $processID = $case['id'];
                    echo "<tr><td>".$i."</td>
                        
                        <td onclick='getScreen(".$processID.")';><option>".$case['id']."</option></td>
                        <td>".$case['element_name']."</td>
                        <td>".$case['status']."</td>
                        <td>".$case['initiated_at']."</td>
                        <td>".$case['due_at']."</td>";
                    
                    }
                ?>
             </table>

                <script type="text/javascript">
                     function getScreen(x)
                     {
                        console.log(x);
                        var solucion =x;

                       document.cookie = escape("selectedTask") + "=" + escape(x);
                       window.open("form.php");
                     }
                </script>




        </div>
<!-- Page Content End-->
<?php include_once("footer.html"); ?>