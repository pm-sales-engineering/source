<!-- REST Connection Start -->
<?php
    session_start();
    if(is_null($_SESSION['access_token']))
        header('Location: index.php');
   
    require_once('executeREST.php');

    $url = $_SESSION['url'].'/api/1.0/'.$_SESSION['ws'].'/project/'.$_GET['proj'].'/activity/'.$_GET['task'].'/steps';
    $steps = executeREST( $url, 'GET', array(), $_SESSION['access_token'] );
// var_dump($steps);
 
    $url = $_SESSION['url'].'/api/1.0/'.$_SESSION['ws'].'/project/'.$_GET['proj'].'/dynaform/'.$steps[0]['step_uid_obj'];
    $dynaform = executeREST( $url, 'GET', array(), $_SESSION['access_token'] );

   $url = $_SESSION['url'].'/api/1.0/'.$_SESSION['ws'].'/cases/'.$_GET['app'].'/variables';
   $variables = executeREST( $url, 'GET', array(), $_SESSION['access_token'] );

 // var_dump($variables);

 // var_dump($dynaform);
?>
<!-- REST Connection End -->

<?php include_once("header.html"); ?>
<!-- Page Content Start-->
        <div>
             <h3>Dynaform: <?php echo $dynaform['dyn_title'] ?></h3>
             <form action="route_case.php?app=<?php echo $_GET['app'] ?>" method="post">
             <table align="center" class="table" style="background-color:#F2F2F2; width: 90%;">
             <?php $rows = json_decode($dynaform['dyn_content'], true);
                    foreach($rows['items'][0]['items'] as $field) {
                        echo "<tr>";
                        for($i = 0 ; $i<=count($field)-1 ; $i++){
                            if(isset($field[$i]['mode']))
                                $mode = $field[$i]['mode'] == 'view'? " readonly" : "";
                          
                            if(isset($field[$i]['var_name']))
                                $value = array_key_exists($field[$i]['var_name'], $variables)? $variables[$field[$i]['var_name']] : "";
                          
                            switch($field[$i]['type']){
                                case "title":
                                    echo "<th style='background-color:#1192d4; color:#FFF; font-size: 17px;' colspan='".$field[$i]['colSpan']."'>".$field[$i]['label']."</th>";
                                    break;
                                case "subtitle":
                                    echo "<th style='background-color:#4EAECE; color:#FFF; font-size: 15px;' colspan='".$field[$i]['colSpan']."'>".$field[$i]['label']."</th>";
                                    break;
                                case "text":
                                
                                    echo "<td colspan='".$field[$i]['colSpan']."'><label>".$field[$i]['label']."</label>
                                              <input name=".$field[$i]['var_name']." value='".$value."' class='form-control'".$mode.">
                                           </td>";
                                    break;
                              
                                case "textarea":
                                    echo "<td colspan='".$field[$i]['colSpan']."'><label>".$field[$i]['label']."</label>
                                              <textarea name=".$field[$i]['var_name']." class='form-control' placeholder='".$field[$i]['placeholder']."' rows='3'".$mode.">".$value."</textarea></td>";
                                    break;
                                case "dropdown":
                                    echo "<td colspan='".$field[$i]['colSpan']."'><label>".$field[$i]['label']."</label><select name=".$field[$i]['var_name']." class='form-control'".$mode.">";
                                    foreach($field[$i]['options'] as $option){
                                      if( $option['value'] == $value )
                                        $selected = " selected";
                                      else
                                        $selected = "";
                                        echo "<option value='".$option['value']."' ".$selected.">".$option['label']."</option>";
                                    }
                                    echo    "</select></td>";
                                    break;
                                case "radio":
                                    echo "<td colspan='".$field[$i]['colSpan']."'><label>".$field[$i]['label']."</label>";
                                    foreach($field[$i]['options'] as $option){
                                      if( $option['value'] == $value )
                                        $selected = " checked";
                                      else
                                        $selected = "";
                                        echo "<div class='radio'><label><input name=".$field[$i]['var_name']." type='radio' value='".$option['value']."'".$mode.$selected.">".
                                                $option['label']."</label></div>";
                                    }
                                    echo    "</td>";
                                    break;
                                case "submit":
                                    echo "<td align='center' colspan='".$field[$i]['colSpan']."'><input class='btn btn-info' type='submit' value='".$field[$i]['label']."'></td>";
                                    break;
                            }
                        }
                        echo "</tr>";
                    }
             ?>
             </table>
             </form>
        </div>
<!-- Page Content End-->
<?php include_once("footer.html"); ?>