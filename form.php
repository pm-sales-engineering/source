<!-- REST Connection Start -->
<?php
 
  session_start();
 
  if(empty($_SESSION))
      header('Location: index.php');
 
  require_once('executeREST.php');
  $TOKEN = $_SESSION['access_token'];
  $url = $_SESSION['url'].'/api/1.0/tasks/'.$_COOKIE['selectedTask'].'?include=data';
  $response = executeREST( $url, 'GET', array(), $_SESSION['access_token'] );
 // var_dump($response);


$json_string=json_encode($response,JSON_PRETTY_PRINT);

$json_data=json_encode($response["data"],JSON_PRETTY_PRINT);

$finalText = explode(',', $json_data); 

?>
<!-- REST Connection End -->

<?php include_once("header.html"); ?>
<!-- Page Content Start-->
        <div style="height: 400px; overflow-y: scroll; padding-left: 50px;">
             <h3>   STEP (<?php echo count($response["data"]); ?>)</h3>
             
                <?php
                //echo "<p contenteditable=true>".$json_data."</p>";
                for($i=0;$i<count($finalText);++$i)
                {
                echo "<p style=text-align:left; contenteditable=true>".$finalText["$i"]."</p>";
                }
                $caseList = $response["data"];
                ?>

                <script type="text/javascript">
                                        
                    import Editor, { DiffEditor, useMonaco, loader } from "@monaco-editor/react";
                    import React, { Component } from "react";

                    render()
                    {
                        return(
                            <div style={{width:'50%',padding:'25px', margin:'0 auto'}}>
                                <Editor 
                                height="600px"
                                theme="vs-dark"
                                language="json"
                                defaultLanguge = "json"/>
                        </div>);
                    }

                </script>
             
        </div>
<!-- Page Content End-->
<?php include_once("footer.html"); ?>