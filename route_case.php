<?php
    session_start();
    if(is_null($_SESSION['access_token']))
        header('Location: index.php');

    require_once('executeREST.php');
    
   $url = $_SESSION['url'].'/api/1.0/'.$_SESSION['ws'].'/cases/'.$_GET['app'].'/variable';
   executeREST( $url, 'PUT', $_POST, $_SESSION['access_token'] );

   $url = $_SESSION['url'].'/api/1.0/'.$_SESSION['ws'].'/cases/'.$_GET['app'].'/route-case';
   executeREST( $url, 'PUT', $_POST, $_SESSION['access_token'] );
   header("Location: list.php");
?>